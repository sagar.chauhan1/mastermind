import masterclass as m


def take_input(msg):
    x = raw_input(msg)
    while not x.isdigit():
        print "Enter the numbers only\n"
        x = raw_input(msg)
    return x


def play(name):
        digit = take_input("Enter the no. of digit in No.  :  ")
        u1 = m.GAME(name, int(digit))
        random_no = u1.generate_no()
        guess_no = int(take_input("How many guesses do you want  : "))
        for i in range(guess_no):
            print "####################################################\n"
            print "Number of try : ", i+1
            n = take_input("Enter the guess. :    ")
            x = u1.high_or_low(int(n), random_no)
            if x == "equal":
                print "Number is Correct"
                break
            else:
                correctno = u1.correct_nos(n, random_no)
                op = u1.correct_pos(n, random_no)
                tmp1 = x+"   "+str(correctno)+" no. are corect   "
                tmp2 = tmp1 + str(op) + "   are in correct position"
                print tmp2
                print "#####################################################\n"
            if i == guess_no - 1:
                print "Name	:	", name, "	Number is : ", random_no


def run_test_cases(name):
    test_numbers = [44440, 1000, 99999, 585154]
    guess_number = ['4040', '85100', '1010', '99899', '85938']
    expected_op = [['Too Low', 3, 3], ['Too High', 1, 1], ['Too Low', 1, 1],
                   ['Too High', 0, 0], ['Too High', 0, 0], ['Too High', 2, 2],
                   ['Too High', 3, 2], ['High', 3, 3], ['Too High', 0, 0],
                   ['Too High', 0, 0], ['Too Low', 0, 0], ['Too Low', 0, 0],
                   ['Too Low', 0, 0], ['Low', 4, 4], ['Too Low', 1, 1],
                   ['Too Low', 1, 0], ['Too Low', 3, 3], ['Too Low', 1, 0],
                   ['Too Low', 1, 0], ['Too Low', 2, 2]]
    for i in range(len(test_numbers)):
        u1 = m.GAME(name, len(str(test_numbers[i])))
        for j in range(len(guess_number)):
            a = u1.high_or_low(int(guess_number[j]), test_numbers[i])
            if a == "equal":
                l = [a]
            else:
                b = u1.correct_nos(guess_number[j], test_numbers[i])
                c = u1.correct_pos(guess_number[j], test_numbers[i])
            l = [a, b, c]
            l1 = expected_op[i*5+j]
            assert l == l1
            print i*5+j+1, "test case run Successfully"

name = raw_input("Enter the Name :	")
choice = take_input("Enter the choice:\n 1:) For test case run \n 2:) For play the game :\n =====>  ")
if int(choice) == 1:
    run_test_cases(name)
else:
    while True:
        play(name)
        reply = raw_input("Do you want to play more  (y/n): ")
        while reply not in ['y', 'Y', 'n', 'N']:
            print("Please input correct keyword  ")
            reply = raw_input("Do you want to play more  (y/n): ")
        if reply == 'y' or reply == 'Y':
            continue
        elif reply == 'n' or reply == 'N':
            break

print "Thank you"
