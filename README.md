**MASTERMIND GAME**

**#How to RUN**
>>> python main.py

## for Crawling the HTML Content
```
python3 main.py -add_data True      or
python3 main.py

```

## forretrieving html content from Database
```
python3 main.py -get_data True

```

## for creating url graph
```
python3 main.py -create_url_map True 

```

## for finding shortest path
```
python3 main.py -shortest_path True 

```

## for Creating Image Database
```
python3 main.py -create_image_database True  

```

## for Trainig Image Classifier
```
python3 main.py -train_nn_model True  

```

Example:-

![alt text](op1.png "Test Cases")

![alt text](op2.png "Runing game")
