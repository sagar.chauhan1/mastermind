import random as r


class GAME:
    def __init__(self, name, digit):
        self.name = name
        self.digit = digit

    def generate_no(self):
        self.number = r.randrange(pow(10, self.digit-1), pow(10, self. digit))
        return self.number

    def high_or_low(self, n, generated_no):
        self.no = generated_no
        self.msg = ""
        if n < self.no - pow(10, self.digit-1):
            self.msg = "Too Low"
        elif n >= self.no - pow(10, self.digit-1) and n < self.no:
            self.msg = "Low"
        elif n > self.no + pow(10, self.digit-1):
            self.msg = "Too High"
        elif n > self.no and n <= self.no + pow(10, self.digit-1):
            self.msg = "High"
        elif n == self.no:
            self.msg = "equal"
        return self.msg

    def correct_nos(self, n, generated_no):
        self.no = generated_no
        s = str(self.no)
        s1 = n
        count = 0
        for i in range(len(s1)):
            if s1[i] in s:
                count += 1
                a = s.index(s1[i])
                s = s[:a]+s[a+1:]
        return count

    def correct_pos(self, n, generated_no):
        self.no = generated_no
        s = n
        s1 = str(self.no)
        count = 0
        x = len(s)
        if len(s1) < len(s):
            x = len(s1)
        i = 1
        while x != 0:
            if s[-i] == s1[-i]:
                count += 1
            i += 1
            x -= 1
        return count
